'use strict';

let _ = require('lodash'),
    apn = require('apn');

function handleNoDevice () {
  console.log('No device, skip push notification.');
}

function handleNotificationsSent(devices) {
  console.log('Notifications sent', devices);
}

function handleSendError(err) {
  console.error('Error sending notification', err);
}

function Notification (_conf) {
  let conf;

  if (!_conf) { throw new Error('no conf was passed.'); }
  if (!_conf.cert || !_conf.key) { throw new Error('Must pass at least a cert and key'); }

  conf = _conf || {};
  this.conf = _.merge({ production: false, cacheLength: 100 }, conf);
  this.con = new apn.Provider(this.conf);
}

Notification.prototype.send = function _send(device, message, options) {
  if (device == undefined || device == null) { return handleNoDevice(); }
  let notification = new apn.Notification();
  _.merge(notification, { sound: 'ping.aiff' }, options, { alert: message });

  if (this.conf.handleLogging)
    return this.con.send(notification, device)
    .then(handleNotificationsSent)
    .catch(handleSendError);

  return this.con.send(notification, device);
};

Notification.prototype.shutdown = function() {
  this.con.shutdown();
};

module.exports = Notification;
